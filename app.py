import csv
import datetime
import hashlib
import secrets
import string
from functools import wraps
from io import TextIOWrapper
import jwt
from flask import Flask, jsonify, request
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
import redis

app = Flask(__name__)
#app.config.from_object('config.Config')
cache = Cache(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:root@172.17.0.2:5432/postgres'
CACHE_REDIS_HOST = "127.0.0.1"
CACHE_REDIS_PORT = 6379
CACHE_REDIS_DB = 0
red = redis.Redis(host=CACHE_REDIS_HOST, port=CACHE_REDIS_PORT, db=CACHE_REDIS_DB)

app.config['SECRET_KEY'] = 'asecretkey'
app.debug = True
db = SQLAlchemy(app)
ma = Marshmallow(app)
error = 'Cannot perform that function'
no_student = 'No student found !'
missing_email = 'Missing Email !'
missing_password = 'Missing Password!'


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing !'}), 401

        try:
            print("Hello")
            JWT_ALGORITHM = 'HS256'
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=JWT_ALGORITHM)

            current_user = teachers.query.filter_by(tid=data['tid']).first()
        except Exception as e:
            print(e)
            return jsonify({'message': 'Token validity over !'}), 401

        return f(current_user, *args, **kwargs)

    return decorated


'''-----------------------------------------CODE FOR CREATING TABLE------------------------------------------------'''


class students(db.Model):
    __tablename__ = "studentlogin"
    sid = db.Column(db.Integer(), primary_key=True, nullable=False)
    sname = db.Column(db.Text(), nullable=False)
    role = db.Column(db.Text(), nullable=False)
    semail = db.Column(db.Text(), nullable=False)
    spass = db.Column(db.Text(), nullable=False)
    salt = db.Column(db.Text(), nullable=False)
    admin = db.Column(db.BOOLEAN(), nullable=False)

    def __init__(self, sid, sname, role, semail, spass, salt, admin):
        self.sid = sid
        self.sname = sname
        self.role = role
        self.semail = semail
        self.spass = spass
        self.salt = salt
        self.admin = admin


class teachers(db.Model):
    __tablename__ = "teacherlogin"
    tid = db.Column(db.Integer(), primary_key=True, nullable=False)
    tname = db.Column(db.Text(), nullable=False)
    temail = db.Column(db.Text(), nullable=False)
    tpass = db.Column(db.Text(), nullable=False)
    salt = db.Column(db.Text(), nullable=False)
    admin = db.Column(db.BOOLEAN(), nullable=False)

    def __init__(self, tid, tname, temail, tpass, salt, admin):
        self.tid = tid
        self.tname = tname
        self.temail = temail
        self.tpass = tpass
        self.salt = salt
        self.admin = admin


class marks(db.Model):
    __tablename__ = "marks"
    sid = db.Column(db.String(), primary_key=True)
    semail = db.Column(db.String())
    physics = db.Column(db.String())
    chemistry = db.Column(db.String())
    maths = db.Column(db.String())
    result = db.Column(db.String())

    def __init__(self, sid, semail, physics, chemistry, maths, result):
        self.sid = sid
        self.semail = semail
        self.physics = physics
        self.chemistry = chemistry
        self.maths = maths
        self.result = result


class StudentSchema(ma.Schema):
    class Meta:
        fields = ('sid', 'sname', 'role', 'semail', 'spass', 'salt', 'admin')


student_schema = StudentSchema()


class TeacherSchema(ma.Schema):
    class Meta:
        fields = ('tid', 'tname', 'temail', 'tpass', 'salt', 'admin')


teacher_schema = TeacherSchema()

class MarksSchema(ma.Schema):
    class Meta:
        fields = ('sid', 'semail', 'physics', 'chemistry', 'maths', 'result')

marks_schema = MarksSchema()

'''-----------------------------------------CODE FOR STUDENT------------------------------------------------------'''


@app.route('/', methods=['GET'])
def hello():
    return jsonify({'msg': 'Hello, Welcome !'})


@app.route('/student', methods=['GET'])
@token_required
def getstudents(current_user):
    if not current_user.admin:
        return jsonify({'message': error})

    all_students = students.query.all()
    output = []
    for student in all_students:
        currstudent = {}
        currstudent['sid'] = student.sid
        currstudent['sname'] = student.sname
        currstudent['role'] = student.role
        currstudent['semail'] = student.semail
        currstudent['spass'] = student.spass
        currstudent['admin'] = student.admin
        output.append(currstudent)
    return jsonify(output)


@app.route('/student', methods=['POST'])
@token_required
def poststudents(current_user):
    studentdata = request.get_json()
    student = students(sid=studentdata['sid'], sname=studentdata['sname'], role=studentdata['role'],
                       semail=studentdata['semail'], spass=studentdata['spass'])
    db.session.add(student)
    db.session.commit()
    return jsonify(studentdata)


@app.route('/student/<sid>', methods=['GET'])
@token_required
def get_student(current_user, sid):
    if not current_user.admin:
        return jsonify({'message': error})
    student = students.query.get(sid)
    print(student.spass)
    return student_schema.jsonify(student)


@app.route('/student/<semail>', methods=['POST'])
def reset_pass(semail):
    semaill = request.json.get('semail', None)
    password = request.json.get('password', None)

    if not semaill:
        return missing_email, 400
    if not password:
        return missing_password, 400

    user = students.query.filter_by(semail=semail).first()

    if str(semaill) == user.semail:
        salt_value = salt_generator()
        new_pass = password + salt_value
        hashed = hashlib.sha256(new_pass.encode()).hexdigest()
        user.salt = str(salt_value)
        user.spass = str(hashed)
        db.session.commit()

        return jsonify({'message': 'The password has been updated !'})


@app.route('/student/<sid>', methods=['DELETE'])
@token_required
def delete_student(current_user, sid):
    if not current_user.admin:
        return jsonify({'message': error})

    user = students.query.filter_by(sid=sid).first()

    if not user:
        return jsonify({'message': no_student})

    db.session.delete(user)
    db.session.commit()

    return jsonify({'message': 'The student has been deleted !'})


@app.route('/registerstudent', methods=['POST'])
@cache.cached(timeout=30, query_string=True)
def register():
    sid = request.json.get('sid', None)
    sname = request.json.get('sname', None)
    role = request.json.get('role', None)
    semail = request.json.get('semail', None)
    password = request.json.get('password', None)

    if not semail:
        return missing_email, 400
    if not password:
        return missing_password, 400

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()

    user = students(sid=sid, sname=sname, role=role, semail=semail, spass=str(hashed), salt=str(salt_value),
                    admin=False)
    red.hset('username', 'sid', sid)
    red.hset('username', 'sname', sname)
    red.hset('username', 'role', role)
    red.hset('username', 'semail', semail)
    red.hset('username', 'spass', str(hashed))
    red.hset('username', 'salt', str(salt_value))
    red.hset('username', 'admin', str(False))

    db.session.add(user)
    db.session.commit()

    return f'Welcome {semail}'


def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)

    for _ in range(6):
        password += secrets.choice(string_source)

    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password


@app.route('/loginstudent', methods=['POST'])
def loginstudent():
    semail = request.json.get('semail', None)
    password = request.json.get('password', None)
    sid = request.json.get("sid", None)
    if not semail:
        return missing_email, 400
    if not password:
        return missing_password, 400
    if not sid:
        return 'Missing ID !', 400

    studentlogin = students.query.filter_by(semail=semail).first()

    if not studentlogin:
        return 'User not found !', 404

    student = students.query.get(sid)
    salt1 = student.salt
    new_pass = password + salt1
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    print(hashed)
    print(student.spass)
    JWT_ALGORITHM = 'HS256'
    if str(hashed) == student.spass:
        token = jwt.encode({'sid': studentlogin.sid, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=1)},
                           app.config['SECRET_KEY'], algorithm=JWT_ALGORITHM)
        print(token)
        return jsonify({'token': token})

    else:
        return 'Wrong Password !'


'''--------------------------------------------CODE FOR TEACHER----------------------------------------------------'''


@app.route('/teacher', methods=['GET'])
@token_required
def getteacher(current_user):
    if not current_user.admin:
        return jsonify({'message': error})

    allTeachers = teachers.query.all()
    output = []
    for teacher in allTeachers:
        currTeacher = {}
        currTeacher['tid'] = teacher.tid
        currTeacher['tname'] = teacher.tname
        currTeacher['temail'] = teacher.temail
        currTeacher['tpass'] = teacher.tpass
        currTeacher['admin'] = teacher.admin
        output.append(currTeacher)
    return jsonify(output)


@app.route('/teacher', methods=['POST'])
@token_required
def postteachers(current_user):
    if not current_user.admin:
        return jsonify({'message': error})
    teacherData = request.get_json()
    teacher = teachers(tid=teacherData['tid'], tname=teacherData['tname'],
                       temail=teacherData['temail'], tpass=teacherData['tpass'])
    db.session.add(teacher)
    db.session.commit()
    return jsonify(teacherData)


@app.route('/teacher/<tid>', methods=['GET'])
@token_required
def get_teacher(current_user, tid):
    if not current_user.admin:
        return jsonify({'message': error})
    teacher = teachers.query.get(tid)
    return teacher_schema.jsonify(teacher)


@app.route('/teacher/<tid>', methods=['PUT'])
@token_required
def promote_teacher(current_user, tid):
    print(current_user.admin)
    if not current_user.admin:
        return jsonify({'message': error})

    user = teachers.query.filter_by(tid=tid).first()

    if not user:
        return jsonify({'message': no_student})

    user.admin = True
    db.session.commit()

    return jsonify({'message': 'The user has been promoted !'})


@app.route('/student/<sid>', methods=['PUT'])
@token_required
def demote_admin(current_user, sid):
    if not current_user.admin:
        return jsonify({'message': error})

    user = students.query.filter_by(sid=sid).first()

    if not user:
        return jsonify({'message': no_student})

    user.admin = False
    db.session.commit()

    return jsonify({'message': 'The user has been Demoted !'})


@app.route('/teacher/<temail>', methods=['POST'])
def reset_password(temail):
    temaill = request.json.get('semail', None)
    password = request.json.get('password', None)

    if not temaill:
        return missing_email, 400
    if not password:
        return missing_password, 400

    user = teachers.query.filter_by(temail=temail).first()

    if str(temaill) == user.temail:
        salt_value = salt_generator()
        new_pass = password + salt_value
        hashed = hashlib.sha256(new_pass.encode()).hexdigest()
        user.salt = str(salt_value)
        user.tpass = str(hashed)
        db.session.commit()

        return jsonify({'message': 'The password has been updated !'})


@app.route('/teacher/<tid>', methods=['DELETE'])
@token_required
def delete_teacher(current_user, tid):
    if not current_user.admin:
        return jsonify({'message': error})

    user = teachers.query.filter_by(tid=tid).first()

    if not user:
        return jsonify({'message': 'No teacher found !'})

    db.session.delete(user)
    db.session.commit()

    return jsonify({'message': 'The teacher  has been deleted !'})


@app.route('/registerteacher', methods=['POST'])
def registerteacher():
    tid = request.json.get('tid', None)
    tname = request.json.get('tname', None)
    temail = request.json.get('temail', None)
    password = request.json.get('password', None)

    if not temail:
        return missing_email, 400
    if not password:
        return missing_password, 400

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()

    user = teachers(tid=tid, tname=tname, temail=temail, tpass=str(hashed), salt=str(salt_value),
                    admin=False)

    db.session.add(user)
    db.session.commit()

    return f'Welcome {temail}'


@app.route('/admin', methods=['POST'])
@token_required
def registeradmin(current_user):
    if not current_user.admin:
        return jsonify({'message': error})
    tid = request.json.get('tid', None)
    tname = request.json.get('tname', None)
    temail = request.json.get('temail', None)
    password = request.json.get('password', None)

    if not temail:
        return missing_email, 400
    if not password:
        return missing_password, 400

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()

    user = teachers(tid=tid, tname=tname, temail=temail, tpass=str(hashed), salt=str(salt_value),
                    admin=True)

    db.session.add(user)
    db.session.commit()

    return f'Welcome {temail}'


@app.route('/loginteacher', methods=['POST'])
def loginteacher():
    temail = request.json.get('temail', None)
    password = request.json.get('password', None)
    tid = request.json.get("tid", None)
    if not temail:
        return 'Missing Email', 400
    if not password:
        return 'Missing Password', 400
    if not tid:
        return 'Missing ID !', 400

    teacherlogin = teachers.query.filter_by(temail=temail).first()

    if not teacherlogin:
        return 'User not found !', 404

    teacher = teachers.query.get(tid)
    salt1 = teacher.salt
    new_pass = password + salt1
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    JWT_ALGORITHM = 'HS256'
    if str(hashed) == teacher.tpass:
        token = jwt.encode(
            {'tid': teacherlogin.tid, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=59)},
            app.config['SECRET_KEY'], algorithm=JWT_ALGORITHM)
        return jsonify({'token': token})

    else:
        return 'Wrong Password !'


@app.route('/upload', methods=['POST'])
def upload(*args):
    if 'file' not in request.files:
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp
    csv_file = request.files['file']
    csv_file = TextIOWrapper(csv_file, encoding='utf-8')
    csv_reader = csv.reader(csv_file, delimiter=',')

    for row in csv_reader:
        sid, semail, physics, chemistry, maths, results = row
        user = marks(sid=sid, semail=semail, physics=physics, chemistry=chemistry, maths=maths,
                     result=results)
        db.session.add(user)
        db.session.commit()

    return "uploaded marks successfully"


@app.route('/checkmarks/<sid>', methods=['POST'])
def checkmarks(sid):
    sidd = request.json.get('sid', None)
    password = request.json.get('password', None)

    if not sidd:
        return 'Missing ID', 400
    if not password:
        return 'Missing Password', 400
    print("Hello")

    user = marks.query.get(sid)
    return marks_schema.jsonify(user)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
